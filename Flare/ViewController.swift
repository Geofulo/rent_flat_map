//
//  ViewController.swift
//  Flare
//
//  Created by Geovanni on 2/21/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import MapboxGeocoder

class ViewController: UIViewController {
    
    @IBOutlet var flareMap: MGLMapView!
    
    let locationManager = CLLocationManager()
    let defaultCameraDistance = CLLocationDistance(floatLiteral: 300.0)
    let defaultCameraPitch : CGFloat = 100.0
    var currentCameraDirection = CLLocationDirection(floatLiteral: 0.0)
    
    let mGeocoder = Geocoder.shared
    //    var apiClient : ZooplaAPIClient?
    var apiClient : NestoriaAPIClient?
    
    var isFirstLocation : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.apiClient = NestoriaAPIClient()
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.headingOrientation = CLDeviceOrientation.portrait
        self.locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
        if CLLocationManager.headingAvailable() {
            self.locationManager.startUpdatingHeading()
        }
    }
}

extension ViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last {
            let camera = MGLMapCamera(lookingAtCenter: userLocation.coordinate, acrossDistance: defaultCameraDistance, pitch: defaultCameraPitch, heading: currentCameraDirection)
            if !isFirstLocation {
                self.isFirstLocation = true
                
                flareMap.fly(to: camera, completionHandler: nil)
                
                let geocoder = CLGeocoder()
                
                sleep(10)
                
                geocoder.reverseGeocodeLocation(userLocation, completionHandler: { (placemarks, error) in
                    if let pm = placemarks?[0] {
                        print("placemark: \(pm.country) - \(pm.isoCountryCode) - \(pm.region)")
                        
                        self.apiClient?.GetProperties(centrePoint: "51.509865,-0.118092", countryCode: "uk", type: "rent", completion: { response in
//                        self.apiClient?.GetProperties(centrePoint: "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)", countryCode: (pm.isoCountryCode?.lowercased())!, type: "rent", completion: { response in
                            if let listingProperties = response?.listings {
                                DispatchQueue.main.async {
                                    for l in listingProperties {
                                        let propertyLocation = CLLocationCoordinate2D(latitude: l.latitude!, longitude: l.longitude!)
    
                                        let annotation = MGLPointAnnotation()
                                        annotation.coordinate = propertyLocation
                                        annotation.title = l.lister_name
                                        
                                        self.flareMap.addAnnotation(annotation)
                                    }
                                    
//                                    let source = MGLShapeSource(identifier: "properties-source", features: features as [MGLFeature] as! [MGLShape & MGLFeature], options: nil)
                                    
//                                    self.flareMap.style?.addSource(source)
                                    
//                                    let layer = MGLFillExtrusionStyleLayer(identifier: "properties", source: source)
//                                    layer.fillExtrusionHeight = NSExpression(forConstantValue: 500)
//                                    layer.fillExtrusionBase = NSExpression(forConstantValue: 0)
//                                    layer.fillExtrusionColor = NSExpression(forConstantValue: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
//                                    layer.fillExtrusionOpacity = NSExpression(forConstantValue: 1)
                                    
//                                    self.flareMap.style?.addLayer(layer)
                                }
                            }
                        })
                    }
                })
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print("new heading... \(newHeading)")
        currentCameraDirection = newHeading.headingAccuracy
    }
}
