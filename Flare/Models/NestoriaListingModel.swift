//
//  NestoriaListingModel.swift
//  Flare
//
//  Created by Geovanni on 2/28/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

struct NestoriaListingModel: Decodable {
//    let bathroom_number: Data?
//    let bedroom_number: Int?
    let car_spaces: Int?
    let commission: Int?
    let construction_year: Int?
    let datasource_name: String?
    let img_height: Int?
    let img_url: String?
    let img_width: Int?
    let keywords: String?
    let latitude: Double?
    let lister_name: String?
    let lister_url: String?
    let listing_type: String?
    let location_accuracy: Int?
    let longitude: Double?
    let price: Float?
    let price_currency: String?
    let price_formatted: String?
    let price_high: Float?
    let price_low: Float?
    let price_type: String?
    let property_type: String?
    let size: Int?
    let size_type: String?
    let summary: String?
    let thumb_height: Int?
    let thumb_url: String?
    let thumb_width: Int?
    let title: String?
    let updated_in_days: Int?
    let updated_in_days_formatted: String?
}
