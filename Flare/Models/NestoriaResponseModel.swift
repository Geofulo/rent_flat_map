//
//  NestoriaResponseModel.swift
//  Flare
//
//  Created by Geovanni on 2/28/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

struct NestoriaAPIResponseModel: Decodable {
    let request: NestoriaRequestModel?
    let response: NestoriaResponseModel?
}

struct NestoriaResponseModel: Decodable {
    let listings: [NestoriaListingModel]?
}

struct NestoriaRequestModel: Decodable {
    
}
