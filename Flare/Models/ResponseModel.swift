//
//  ResponseModel.swift
//  Flare
//
//  Created by Geovanni on 2/26/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

struct ResponseModel: Codable {
    let area_name: String?
    let country: String?
//    let county: String?
    let latitude: String?
    let longitude: String?
    let result_count: Int?
    let listing: [ListingModel]?
}
