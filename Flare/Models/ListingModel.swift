//
//  ListingModel.swift
//  Flare
//
//  Created by Geovanni on 2/26/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

struct ListingModel: Codable {
    let agent_address: String?
    let agent_logo: String?
    let agent_name: String?
    let agent_phone: String?
    let category: String?
    let country: String?
    let country_code: String?
    let county: String?
    let description: String?
    let short_description: String?
    let details_url: String?
    let displayable_address: String?
    let first_published_date: String?
    let furnished_state: String?
    let image_150_113_url: String?
    let image_354_255_url: String?
    let image_50_38_url: String?
    let image_645_430_url: String?
    let image_80_60_url: String?
    let image_caption: String?
    let image_url: String?
    let last_published_date: String?
    let latitude: Double?
    let longitude: Double?
    let listing_id: String?
    let listing_status: String?
    let location_is_approximate: Int?
    let num_bathrooms: String?
    let num_bedrooms: String?
    let num_floors: String?
    let num_recepts: String?
    let outcode: String?
    let post_town: String?
//    let price: String?
    let property_type: String?
    let status: String?
    let street_name: String?
    let thumbnail_url: String?
}
