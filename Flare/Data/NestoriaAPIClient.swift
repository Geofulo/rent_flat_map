//
//  NestoriaAPIClient.swift
//  Flare
//
//  Created by Geovanni on 2/28/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

class NestoriaAPIClient {
    private let urlString = "https://api.nestoria.co.uk/api?encoding=json&pretty=1"
    private let propertiesEndpoint = "search_listings"
    
    func GetProperties(centrePoint: String, countryCode: String, type: String, completion: @escaping (NestoriaResponseModel?) -> ()) {
        if let url = URL(string: "\(urlString)&action=\(propertiesEndpoint)&listing_type=\(type)&centre_point=\(centrePoint)&number_of_results=\(50)") {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Response: \(response))")
                
                guard let dataResponse = data, error == nil else {
                    print("Response Error: \(String(describing: error?.localizedDescription))")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(NestoriaAPIResponseModel.self, from: dataResponse)
                    
                    completion(res.response)
                    return
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(nil)
                    return
                }
            })
            
            task.resume()
        }
        else {
            print("Not valid URL")
        }
    }
    
}
