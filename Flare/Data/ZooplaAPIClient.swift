//
//  APIClient.swift
//  Flare
//
//  Created by Geovanni on 2/26/19.
//  Copyright © 2019 Flare. All rights reserved.
//

import Foundation

class ZooplaAPIClient {
    private let urlString = "http://api.zoopla.co.uk/api/v1/"
    private let propertiesEndpoint = "property_listings.json"
    private let apiKey = "##############"
    
    func GetProperties(postcode: String, completion: @escaping (ResponseModel?) -> ()) {
        if let url = URL(string: "\(urlString)\(propertiesEndpoint)?api_key=\(apiKey)&area=\(postcode)") {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Response: \(response))")
                
                guard let dataResponse = data, error == nil else {
                    print("Response Error: \(String(describing: error?.localizedDescription))")
                    return 
                }
                do {
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(ResponseModel.self, from: dataResponse)
                    
                    print("Response JSON: \(res))")
                    
                    completion(res)
                    return
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(nil)
                    return
                }
            })
            
            task.resume()
        }
        else {
            print("Not valid URL")
        }
        
    }
    
    func GetProperties(latitude: String, longitude: String, completion: @escaping (ResponseModel?) -> ()) {
        if let url = URL(string: "\(urlString)\(propertiesEndpoint)?api_key=\(apiKey)&latitude=\(latitude)&longitude=\(longitude)") {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Response: \(response))")
                
                guard let dataResponse = data, error == nil else {
                    print("Response Error: \(String(describing: error?.localizedDescription))")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(ResponseModel.self, from: dataResponse)
                    
                    print("Response JSON: \(res))")
                    
                    completion(res)
                    return
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(nil)
                    return
                }
            })
            
            task.resume()
        }
        else {
            print("Not valid URL")
        }
        
    }
}

